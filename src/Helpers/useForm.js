import {useState} from 'react'

export const useForm = (initialState = {} )=> {
  const [values, setValues] = useState(initialState);
  const handleInputChange = ({target}) => {
      setValues({
       ...values, 
      [target.name]: (target.value === 'true' || target.value === 'false') ?
                          (target.value === "true") ? 
                              true : false : 
                              target.value
      })
  };
return[values, handleInputChange, setValues];
}