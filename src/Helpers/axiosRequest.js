import axios from "axios";

const baseUrl = process.env.URL;
export const axiosRequest = async ({endpoint, method = 'get', headers = '', data = ''}) => {

  const token = localStorage.getItem('token') || '';
  let response;
  try{
  if(method !== 'get'){
    
    response = await axios.request({
      url: `${baseUrl}${endpoint}`,
      method,
      data,
      headers : {...headers, 'x-access-token': token}
    })
  }
  else{
    response = await axios.request({
      url: `${baseUrl}${endpoint}`,
      method,
      headers : {...headers, 'x-access-token': token}
    })  
  }
  }
  catch(e){
    response = {
      status: 400,
      data: ""
    }
  }
  finally{
    return response;
  }
}
