import React from "react";
import RegisterHome from "../Modals/RegisterHome";
import styled from "styled-components";
import Footer from "../Components/Footer";
import DashboardAnimation from "../Components/DashboardAnimation";
import GlobalStyles from "../GlobalStyles";

const Home = styled.div`
  background-color: #f9f9f9;
  height: 100vh;
  width: 100vw;
  overflow: hidden;

  .main {
    display: grid;
    position: relative;
    margin-left: 14em;
  }

  h4 {
    color: rgba(0, 0, 0, 0.8);
    font-weight: 600;
  }
`;

const Dashboard = () => {
  return (
    <>
      <GlobalStyles />
      <Home>
        <section className="main center">
          <h1>
            Support the creators <br />
            you love, by shopping
            <br />
            from ther lists.
          </h1>
          <h4>
            Introducing lilStores - An experiment from <br />
            Coolhumans Studio to help creators monetize from <br />
            their product curation.
          </h4>
          <section className="button-container">
            <button className="btn">Explore Now</button>
            <RegisterHome />
          </section>
        </section>
        <DashboardAnimation />
      </Home>
    </>
  );
};

export default Dashboard;
