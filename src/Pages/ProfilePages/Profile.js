import React from "react";
import NewProduct from "../../Modals/NewProduct";
import TableProfile from "../../Components//TableProfile";
import EditProfile from "../../Modals/EditProfile";
import styled from "styled-components";
import GlobalStyles from "../../GlobalStyles";
import UserPhoto from "../../Images/Userphoto.png";

const Profile = styled.div`
  .banner {
    background-color: #e5e5e5;
    min-height: 220px;
    width: 100vw;

    img {
      width: 120px;
      min-height: 120px;
      border-radius: 50%;
      position: relative;
      left: 50%;
      top: 220px;
      transform: translate(-50%, -50%);
    }
  }

  .button-container {
    padding-top: 3.5em;
    justify-content: center;
  }

  .user-info {
    padding-top: 3.5em;
    padding-bottom: 2em;

    h3,
    h4 {
      text-align: center;
    }
  }

  h4 {
    font-weight: bold;
    line-height: 0em;
    margin: -1em;
    color: rgba(0, 0, 0, 0.3);
  }

  h3 {
    line-height: 1.61em;
  }
`;

const ProfileLogged = () => {
  return (
    <Profile>
      <GlobalStyles />
      <header>
        <section className="banner">
          <img src={UserPhoto} alt="profile"></img>
        </section>
        <section className="user-info">
          <h3>Username</h3>
          <h4>@username</h4>
          <div className="button-container">
            <NewProduct />
            <EditProfile />
          </div>
        </section>
      </header>
      <section>
        <TableProfile />
      </section>
    </Profile>
  );
};

export default ProfileLogged;
