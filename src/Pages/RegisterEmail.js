import React, { useState } from "react";
import { axiosRequest } from "../Helpers/axiosRequest";
import Swal from "sweetalert2";
import { useForm } from "../Helpers/useForm";
import GlobalStyles from "../GlobalStyles";
import styled from "styled-components";

const Form = styled.form`
  input {
    width: 17.22em;
  }
`;

const RegisterEmail = () => {
  const [formValues, handleInputChange] = useForm({
    name: "",
    username: "",
    email: "",
    password: ""
  });

  const { name, username, email, password } = formValues;

  const sendToEndpoint = (e) => {
    const request = {
      endpoint: "/user/new",
      method: "post",
      headers: { "X-Requested-With": "XMLHttpRequest" },
      data: formValues
    };
    console.log(formValues);
    axiosRequest(request);
    Swal.fire({
      title: "Enviado!",
      icon: "success",
      confirmButtonText: "Ok"
    });
  };

  return (
    <div className="center">
      <GlobalStyles />
      <Form onSubmit={sendToEndpoint.bind(this)}>
        <h3>Register now and share your products.</h3>
        <input
          name="name"
          required
          value={name}
          placeholder="What's your name?"
          onChange={handleInputChange}
        ></input>
        <input
          name="username"
          requerid
          value={username}
          placeholder="@username"
          onChange={handleInputChange}
        ></input>
        <input
          name="email"
          required
          value={email}
          placeholder="Enter your email"
          onChange={handleInputChange}
        ></input>
        <input
          name="password"
          required
          value={password}
          type="password"
          placeholder="Enter your password"
          onChange={handleInputChange}
        ></input>
        <button className="btn">Register</button>
      </Form>
    </div>
  );
};

export default RegisterEmail;
