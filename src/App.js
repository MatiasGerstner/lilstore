import Dashboard from "./Pages/Dashboard";
import RegisterEmail from "./Pages/RegisterEmail";
import Profile from "./Pages/ProfilePages/Profile";
import { BrowserRouter as Router, Route } from "react-router-dom";

function AppRouter() {
  return (
    <Router>
      <div className={"site-content"}>
        <Route path="/" exact component={Dashboard} />
        <Route path="/RegisterEmail/" component={RegisterEmail} />
        <Route path="/Profile/" component={Profile} />
      </div>
    </Router>
  );
}

export default AppRouter;
