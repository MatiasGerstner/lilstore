import React from "react";
import styled from "styled-components";

//product img
import img1 from "../Images/Dashboard/2x/Group_13.png";
import img2 from "../Images/Dashboard/2x/Group_14.png";
import img3 from "../Images/Dashboard/2x/Group_15.png";
import img4 from "../Images/Dashboard/2x/Group_16.png";
import img5 from "../Images/Dashboard/2x/Group_17.png";
import img6 from "../Images/Dashboard/2x/Group_18.png";
import img7 from "../Images/Dashboard/2x/Group_19.png";
import img8 from "../Images/Dashboard/2x/Group_20.png";
import img9 from "../Images/Dashboard/2x/Group_23.png";
import img10 from "../Images/Dashboard/2x/Group_24.png";

const AnimationBoard = styled.div`
  top: -20em;
  left: 60%;
  position: relative;
  width: 10vw;

  .main-grid {
    left: 1000px;
    width: inherit;
    display: grid;
    grid-template-columns: repeat(2, 1fr);

    .grid-one {
      display: grid;
      grid-template-rows: repeat(5, 1fr);
      animation-name: leftbar;
      animation-duration: 8s;
      animation-timing-function: ease;
      transform: translateY(10px);
    }

    .grid-two {
      display: grid;
      grid-template-rows: auto;
      animation-name: rightbar;
      animation-duration: 8s;
      animation-timing-function: ease;
      transform: translateY(-80px);
    }

    img {
      width: 250px;
      overflow: hidden;
    }

    @keyframes leftbar {
      from {
        transform: translateY(-200px);
      }
      to {
        transform: translateY(10px);
      }
    }

    @keyframes rightbar {
      from {
        transform: translateY(100px);
      }
      to {
        transform: translateY(-80px);
      }
    }
  }
`;

function DashboardAnimation() {
  return (
    <AnimationBoard>
      <section className="main-grid">
        <div className="grid-one">
          <img src={img1} alt="creator-product"></img>
          <img src={img2} alt="creator-product"></img>
          <img src={img3} alt="creator-product"></img>
          <img src={img4} alt="creator-product"></img>
          <img src={img5} alt="creator-product"></img>
        </div>
        <div className="grid-two">
          <img src={img6} alt="creator-product"></img>
          <img src={img7} alt="creator-product"></img>
          <img src={img8} alt="creator-product"></img>
          <img src={img9} alt="creator-product"></img>
          <img src={img10} alt="creator-product"></img>
        </div>
      </section>
    </AnimationBoard>
  );
}

export default DashboardAnimation;
