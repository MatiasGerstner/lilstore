import React from "react";
import styled from "styled-components";
import NewProduct from "../Modals/NewProduct";

const EmptyBoard = styled.div`
  display: flex;
  background-color: #f2f2f2;
  position: relative;
  width: 80%;
  left: 10%;
  height: 30em;
  border-radius: 20px;
  align-items: center;
  justify-content: center;
  text-align: center;

  .btn {
    margin: auto;
  }

  p {
    font-weight: bold;
    font-size: 0.8em;
    color: rgba(0, 0, 0, 0.7);
  }
`;

function EmptyProfile() {
  return (
    <EmptyBoard>
      <div>
        <p>
          {" "}
          You haven't added any products <br /> to your board yet.
        </p>
        <NewProduct />
      </div>
    </EmptyBoard>
  );
}

export default EmptyProfile;
