import React from "react";
import styled from "styled-components";
import Card from "../Components/Card";
import EmptyProfile from "../Components/EmptyProfile";

// fake data binding to render different views when there are products or not.
let data = [];

const Board = styled.div`
  min-height: 40vh;

  .main-grid {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    align-items: center;
    justify-items: center;
    gap: 1.5em;
    width: 80vw;
    height: auto;
    position: relative;
    left: 10%;
  }
`;

const TableProfile = () => (
  <Board>
    {data.length ? (
      <section className="main-grid">
        <Card url="#" product="Resurrection hand wash" brand="AESOP" />
      </section>
    ) : (
      <EmptyProfile />
    )}
  </Board>
);

export default TableProfile;
