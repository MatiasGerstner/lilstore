import React from 'react'
import {
  Container,
  Grid,
  Header,
  List,
  Segment,
} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'

const Footer = () => (
  
    <Segment inverted vertical style={{ padding: '5em 0em' }}>
      <Container>
        <Grid divided inverted stackable>
          <Grid.Row>
            <Grid.Column width={3}>
              <Header inverted as='h4' content='About' />
              <List link inverted>
                <List.Item as='a'>Sitemap</List.Item>
                <List.Item as='a'>Contact Us</List.Item>
                <List.Item as='a'>About</List.Item>
                <List.Item as='a'>Privacy</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={3}>
              <Header inverted as='h4' content='Services' />
              <List link inverted>
                <List.Item as='a'>Api</List.Item>
                <List.Item as='a'>FAQ</List.Item>
                <List.Item as='a'>Help</List.Item>
                <List.Item as='a'>Terms</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={7}>
              <Header as='h4' inverted>
              headquarters
              </Header>
              <p>
              Villa gesel 2020 - 
                Cordoba, Argentina
              </p>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </Segment>
  
)
 
  export default Footer
  