import React from "react";
import styled from "styled-components";
import Photo from "../Images/Userphoto.png";
import GlobalStyles from "../GlobalStyles";

const CardStyles = styled.div`
  width: 18em;
  height: fit-content;

  h1 {
    font-size: 1.3em;
    line-height: 1.21em;
  }

  h3 {
    font-size: 0.8em;
    text-transform: uppercase;
    color: rgba(0, 0, 0, 0.33);
  }

  h1,
  h3 {
    margin: 0.5em 0;
  }

  img {
    height: 100%;
    width: 100%;
    border-radius: 12px 12px 0px 0px;
  }

  button {
    width: 100%;
  }
`;

function Card({ url, brand, product }) {
  return (
    <CardStyles>
      <GlobalStyles />
      <img src={Photo} alt="product"></img>
      <h3>{brand}</h3>
      <h1>{product}</h1>
      <button className="btn" href={url}>
        Shop this product
      </button>
    </CardStyles>
  );
}

export default Card;
