import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`

:root {
  --light-gray: #e5e5e5;
  --gray: #F2F2F2;
  --dark-gray: rgba(0, 0, 0, 0.26);
  --border-color: rgba(0, 0, 0, 0.1);
}


* {
  padding: 0;
  margin: 0;
}

html {
  font-size: 18px !important;
}

html,
body {
  min-height: 100%;
}

body {
  height: 100vh;
  color: black;
  top: 0;
  position: absolute;
  //overflow: hidden;
  font-size: 1em;
}

input {
  display: flex;
  font-family: "Inter";
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0.56em 1.67em;
  background-color: var(--gray);
  border-radius: 8px;
  border: none;
  margin: 1em 0;
  
}

:-ms-input-placeholder, ::placeholder {
  color: var(----dark-gray);
}

h1, h2, h3, h4, h5, p {
  font-family: 'Inter';
}

h1 {
  font-style: normal;
  font-size: 42px;
  letter-spacing: -0.025em;
  margin: 0.25em 0;
}

h4 {
  font-weight: 500;
  font-size: 16px;
  margin: 0.5rem 0;
  margin-bottom: 1rem;
  line-height: 1rem;
}

.btn {
  background-color: white;
  color: black;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  height: 52px;
  justify-content: center;
  align-items: center;
  padding: 10px 30px;
  border-radius: 8px;
  font-family: 'Inter';
  font-weight: bold;
  border: 1px solid var(--border-color);
  font-weight: 700;
  width: fit-content;
  margin-right: 1rem;
  font-size: 0.8em;
}

.btn-black {
  color: var(--light-gray);
  background-color: #000000;
}

.button-container {
  display: flex;
}

body .modal {
  background-color: red;
  background-color: white;
  border: 1px solid var(--border-color);
  border-radius: 20px !important;
  height: fit-content;
  font-size: 1em;
  padding: 4em;
  text-align: center;
  z-index: 1;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  overflow: hidden;
}

.modal-xl.modal-xl {
  width: 90vw;
  height: 33.33.em !important;
}

.actions {
  display: none;
}

.center{
  margin: auto;
  position: absolute;
  left: 50vw;
  top: 50%;
  transform: translate(-50%, -50%);
}

textarea {
    border: none;
    background: var(--gray);
    border-radius: 8px;
    width: 100%;
    height: 5em;
    padding: 0.56em 1.67em;
  }

  .btn, textarea, input {
    font-size: 0.8em;
  }
`;

export default GlobalStyles;
