import React from "react";
import { Image, Modal } from "semantic-ui-react";
import UserPhoto from "../Images/Userphoto.png";
import styled from "styled-components";
import GlobalStyles from "../GlobalStyles";
import { useForm } from "../Helpers/useForm";
import { axiosRequest } from "../Helpers/axiosRequest";
import Swal from "sweetalert2";

const FormStyles = styled.div`
  display: grid;
  grid-gap: 0.2em;
  align-items: center;
  justify-items: center;
  margin: auto;

  button {
    margin: 0.5em 0;
  }

  form > button {
    width: 100%;
    background: #31c85b;
    color: white;
  }
`;
function exampleReducer(state, action) {
  switch (action.type) {
    case "OPEN_MODAL":
      return { open: true, dimmer: action.dimmer };
    case "CLOSE_MODAL":
      return { open: false };
    default:
      throw new Error();
  }
}

const EditProfile = () => {
  const [state, dispatch] = React.useReducer(exampleReducer, {
    open: false,
    dimmer: undefined
  });

  const [formValues, handleInputChange] = useForm({
    name: "",
    username: "",
    bio: "",
    instagramProfile: ""
  });

  const { name, username, bio, instagramProfile } = formValues;

  const sendToEndpoint = (e) => {
    const request = {
      endpoint: "/user/edit",
      method: "post",
      headers: { "X-Requested-With": "XMLHttpRequest" },
      data: formValues
    };
    console.log(formValues);
    axiosRequest(request);
    Swal.fire({
      title: "Enviado!",
      icon: "success",
      confirmButtonText: "Ok"
    });
  };

  const { open, dimmer } = state;

  return (
    <div>
      <GlobalStyles />
      <Modal
        style={{ width: "27em" }}
        className="modal"
        dimmer={dimmer}
        trigger={
          <button
            className="btn"
            onClick={() => dispatch({ type: "OPEN_MODAL", dimmer: "blurring" })}
          >
            Edit Profile
          </button>
        }
      >
        <FormStyles>
          <h2>Edit your profile</h2>
          <Image src={UserPhoto} size="small" circular />
          <button className="btn btn-black">Upload a picture</button>
          <form onSubmit={sendToEndpoint.bind(this)}>
            <input
              name="name"
              value={name}
              onChange={handleInputChange}
              placeholder="First Name"
            />
            <input
              name="username"
              value={username}
              onChange={handleInputChange}
              placeholder="@Username"
            />
            <input
              name="instagramProfile"
              value={instagramProfile}
              onChange={handleInputChange}
              placeholder="Instagram profile"
            />
            <textarea
              name="bio"
              value={bio}
              onChange={handleInputChange}
              placeholder="Write a short bio"
            />
            <button className="btn ">Save changes</button>
          </form>
        </FormStyles>
      </Modal>
    </div>
  );
};

export default EditProfile;
