import React from "react";
import { Modal } from "semantic-ui-react";
import UserPhoto from "../Images/Userphoto.png";
import GlobalStyles from "../GlobalStyles";
import styled from "styled-components";

const ModalStyles = styled.form`
  min-width: 80vw;

  section.button-container:first-child {
    position: absolute;
    right: 0;
    top: 0;
    margin: 1em;
    z-index: -1;
  }

  img {
    display: block;
    position: absolute;
    left: 0;
    height: 100%;
    top: 0;
    border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;
    min-width: 27.78em;
    z-index: 2;
  }

  .form-fields {
    display: block;
    position: relative;
    left: 40%;

    p,
    .btn {
      color: rgba(0, 0, 0, 0.26);
    }

    p {
      font-weight: bold;
    }

    .btn {
      background-color: #f2f2f2;
      border: none;
    }
  }
`;

function exampleReducer(state, action) {
  switch (action.type) {
    case "OPEN_MODAL":
      return { open: true, dimmer: action.dimmer };
    case "CLOSE_MODAL":
      return { open: false };
    default:
      throw new Error();
  }
}

function NewProduct() {
  const [state, dispatch] = React.useReducer(exampleReducer, {
    open: true,
    dimmer: undefined
  });
  const { open, dimmer } = state;

  return (
    <div>
      <GlobalStyles />
      <Modal
        style={{ top: "70%" }}
        className="modal modal-xl modal-xl modal-xl"
        dimmer={dimmer}
        trigger={
          <button
            className="btn btn-black"
            onClick={() => dispatch({ type: "OPEN_MODAL", dimmer: "blurring" })}
          >
            + Add new product
          </button>
        }
      >
        <ModalStyles>
          <section>
            <section className="button-container">
              <button className="btn">Cancel</button>
              <button className="btn btn-black">+ Add new product</button>
            </section>
            <section className="form-fields">
              <h2>Add a new product</h2>
              <input placeholder="Name of the product" />
              <input placeholder="Name of the brand" />
              <input placeholder="$ 0.00" />
              <input placeholder="Link to the product" />
              <p>Is this an affiliate product?</p>
              <section className="button-container">
                <button className="btn">Yes</button>
                <button className="btn">No</button>
              </section>
            </section>
          </section>

          <img src={UserPhoto} alt="product img" />
        </ModalStyles>
      </Modal>
    </div>
  );
}

export default NewProduct;
