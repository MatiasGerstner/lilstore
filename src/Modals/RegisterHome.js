import React from "react";
import { Modal } from "semantic-ui-react";
import { GoogleLogin } from "react-google-login";

function exampleReducer(state, action) {
  switch (action.type) {
    case "OPEN_MODAL":
      return { open: true, dimmer: action.dimmer };
    case "CLOSE_MODAL":
      return { open: false };
    default:
      throw new Error();
  }
}

function EditProfie() {
  const [state, dispatch] = React.useReducer(exampleReducer, {
    open: false,
    dimmer: undefined
  });
  const { open, dimmer } = state;

  const responseGoogle = (response) => {
    console.log(response);
  };

  return (
    <div>
      <Modal
        dimmer={dimmer}
        open={open}
        onClose={() => dispatch({ type: "CLOSE_MODAL" })}
        className="modal"
        style={{ width: "41.67em" }}
        trigger={
          <button
            onClick={() => dispatch({ type: "OPEN_MODAL", dimmer: "blurring" })}
            className="btn btn-black"
          >
            Are you a creator? Join now!
          </button>
        }
      >
        <h2>
          Register and share your products <br /> with your audience.
        </h2>
        <section className="button-container">
          <button className="btn">Continue with email</button>
          <GoogleLogin
            clientId="831142271605-qmmfne310oacrasvacosp2gupoj8h9c6.apps.googleusercontent.com"
            render={(renderProps) => (
              <button
                className="btn"
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
              >
                Continue with Google
              </button>
            )}
            buttonText="Login"
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            cookiePolicy={"single_host_origin"}
          />
        </section>

        <Modal.Actions></Modal.Actions>
      </Modal>
    </div>
  );
}

export default EditProfie;
